import unittest
from handan import LogSaver
from handan import logMachine

logs = LogSaver()


@logMachine
def testSum(a, b):
    return a * b


print(logs.error('%s: This tests the ERROR logging call.') % testSum(3, 5))


@logMachine
def testABC(a, b):
    return a + b


print(logs.warn('%s: This tests the WARN logging call.') % testABC(5, 10))


@logMachine
def testOther(a):
    if a == 'blah':
        return a


print(logs.info('%s: This is tests the INFO logging call.') % testOther('blah'))


@logMachine
def testA(normal):
    if normal == 'normal':
        return normal


print(logs.normal('%s: This is tests the NORMAL logging call.') %
      testA('normal'))


@logMachine
def testB(good):
    if good == 'g':
        return good


print(logs.good('%s: This is test the GOOD logging call.') % testB('g'))


class HandanTest(unittest.TestCase):
    def errorTest(self):
        self.assertEqual(testSum(3, 5), 15)

    def warnTest(self):
        self.assertEqual(testABC(5, 10), 15)

    def infoTest(self):
        self.assertEqual(testOther('blah'), 'blah')

    def normalTest(self):
        self.assertEqual(testA('normal'), 'normal')

    def goodTest(self):
        self.assertEqual(testB('g'), 'g')


if __name__ == '__main__':
    unittest.main()
