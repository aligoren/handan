from distutils.core import setup

setup(
    name = 'handan',
    version = '0.1',
    url = 'https://github.com/aligoren/handan',
    download_url = 'https://github.com/aligoren/handan/archive/master.zip',
    author = 'Ali GOREN <goren.ali@yandex.com>',
    author_email = 'goren.ali@yandex.com',
    license = 'Apache v2.0 License',
    packages = ['handan'],
    description = 'Simply Logging Library',
    long_description = file('README.md', 'r').read(),
    keywords = ['logging', 'log', 'logserver', 'logger'],
)
