**handan** is a simply logging library. Written in Python

#Installation:

`python setup.py install`

#Usage:

~~~~{.python}
from handan import LogSaver
from handan import logMachine

@logMachine
def test(a,b):
    return a*b

test(3,6)

logs = LogSaver()

print(logs.error('This is tests the ERROR logging call'))
~~~~

#License:

```
   Apache v2.0 License
   Copyright 2014-2015 Martin Simon

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
```
