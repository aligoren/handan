from datetime import datetime

__title__ = 'handan'
__version__ = '0.1'
__author__ = 'Ali GOREN <goren.ali@yandex.com>'
__repo__ = 'https://github.com/aligoren/handan'
__license__ = 'Apache v2.0 License'


class LogSaver(object):
    """
    Log Levels Declared
    ERROR
    WARN = WARNING
    INFO
    NORMAL
    GOOD
    FUNC = FUNCTION
    """
    ERROR = '{0}'.format('\033[91m[ERROR]\033[0m :: ')
    WARN = '{0}'.format('\033[93m[WARN]\033[0m :: ')
    INFO = '{0}'.format('\033[94m[INFO]\033[0m :: ')
    NORMAL = '{0}'.format('\033[0m[NORMAL]\033[0m :: ')
    GOOD = '{0}'.format('\033[92m[GOOD]\033[0m :: ')
    FUNC = '{0}{1}'.format('\033[95m[FUNC]\033[0m', ' :: ')

    @staticmethod
    def tstamp():
        """ 
        tstamp == timestamp 
        tnow = time now
        tregular = time ordered
        """
        tnow = datetime.now()
        tregular = datetime.strftime(tnow, '%d-%m-%Y %H:%M:%S')
        return ('{0}{1}'.format(tregular, ' :: '))

    @classmethod
    def f_call(self, fn, retval, *args):
        """ 
        Call Function
        self.tstamp() => tstamp
        self.FUNC => FUNC
        """
        print('{0}{1}{2}{3}{4}{5}'.format(self.tstamp(), self.FUNC, fn.__name__,
                                          retval, ' returned ', str(*args)))

    @classmethod
    def error(self, logText):
        """
        Log Level Name: Error
        Return: => self.tstamp(), self.ERROR, logText
        """
        return ('{0}{1}{2}'.format(self.tstamp(), self.ERROR, logText))

    @classmethod
    def warn(self, logText):
        """
        Log Level Name: Warn
        Return: => self.tstamp(), self.WARN, logText
        """
        return ('{0}{1}{2}'.format(self.tstamp(), self.WARN, logText))

    @classmethod
    def info(self, logText):
        """
        Log Level Name: Info
        Return: => self.tstamp(), self.INFO, logText
        """
        return ('{0}{1}{2}'.format(self.tstamp(), self.INFO, logText))

    @classmethod
    def normal(self, logText):
        """
        Log Level Name: Normal
        Return: => self.tstamp(), self.NORMAL, logText
        """
        return ('{0}{1}{2}'.format(self.tstamp(), self.NORMAL, logText))

    @classmethod
    def good(self, logText):
        """
        Log Level Name: Good
        Return: => self.tstamp(), self.GOOD, logText
        """
        return ('{0}{1}{2}'.format(self.tstamp(), self.GOOD, logText))


def logMachine(fn):
    """
    Decorator: logMachine
    r_val => return value
    returns => wrap => return r_val, logMachine => wrap
    """

    def wrap(*args):
        r_val = fn(*args)
        LogSaver.f_call(fn, args, r_val)
        return r_val

    return wrap
