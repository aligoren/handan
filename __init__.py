__title__ = 'handan'
__version__ = '0.1'
__author__ = 'Ali GOREN <goren.ali@yandex.com>'
__repo__ = 'https://github.com/aligoren/handan'
__license__ = 'Apache v2.0 License'

from .handan import LogSaver
from .handan import logMachine
